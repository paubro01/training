# Setting Model training

## Introduction
This project provides step by step setup of model training either:
- locally (README_Local_Model_Training_Setup).
- on AWS (README_AWS_Model_Training_Setup).
- on Alibaba Cloud (README_AliCloud_Model_Training_Setup).

Select the preferred option depending of your preference.

It is recommended to explore and use the **traffic-monitoring > application-microservices** projects first.

## Pre-Requisites
Depending on the user choice, the user might need an AWS or Alibaba Cloud account.

## Setup Guide
Clone the project.
Run the command given below to clone the project.
```sh
git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

Directory Structure for cloned project
- <PROJECT_ROOT>
    - cloud_config/
    - dataset_downloader/
    - docs/
    - helper/
    - model_data/
    - dataset_downloader.py
    - Dockerfile_Alibaba
    - Dockerfile_AWS
    - Dockerfile_Local_training
    - README_AliCloud_Model_Training_Setup.md
    - README_AWS_Model_Training_Setup.md
    - README_Local_Model_Training_Setup.md
    - train_model.py

## What's next ?
- Refer to the Model compilation repository README document.